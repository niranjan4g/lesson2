module.exports = {
    '@tags': ['google'],
    'Google advanced search: Elon Musk'(browser) {

        const URL = 'https://www.google.com/advanced_search';
        const mainQuery = 'Elon Musk';
        const mainQueryInputSelector = 'input[name="as_q"]';
        const languageDropDownOpenerSelector = '#lr_button';
        const languageDropDownValueSelector = '.goog-menuitem[value="lang_it"]';
        const lastUpdateDropDownOpenerSelector = '#as_qdr_button';
        const lastUpdateDropDownValueSelector = '.goog-menuitem[value="m"]';
        const submitButtonSelector = '.jfk-button[type="submit"]';

        const resultPageQuerySelector = `#searchform input[name="q"][value="${mainQuery}"]`;

        browser
            .url(URL)
            .waitForElementVisible('body', 2000)
            .setValue(mainQueryInputSelector, mainQuery)
            .pause(1000)
            .click(languageDropDownOpenerSelector)
            .click(languageDropDownValueSelector)
            .pause(1000)
            .click(lastUpdateDropDownOpenerSelector)
            .click(lastUpdateDropDownValueSelector)
            .pause(1000)
            .click(submitButtonSelector)
            .assert.urlContains('as_q=Elon+Musk', 'Query is Elon Musk')
            .assert.urlContains('lr=lang_it', 'Language is Italian')
            .assert.urlContains('as_qdr=m', 'Last Update Time is a month ago')
           

        browser 
            .assert.visible(resultPageQuerySelector, 'UI: Elon Musk in the query Input')
           
        browser
            .saveScreenshot('tests_output/google.png')
    }
}